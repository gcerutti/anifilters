import logging
import subprocess
from os import path
from tempfile import TemporaryDirectory

import numpy as np
from timagetk.components.spatial_image import SpatialImage
from timagetk.io import imread
from timagetk.io import imsave


def ani3d(image: SpatialImage, K: float = 0.2, sigma: float = 4, gamma: float = 1., upDir: int = 10, nbIter: int = 50) \
        -> SpatialImage:
    """Apply an anisotropic filter on an intensity image.

    Calls the ``ani3D`` executable.

    Parameters
    ----------
    image : SpatialImage
        input 3d greyscale intensity image in SpatialImage format from timagetk
    K : float, default=0.2
        proportional to the cell walls width [0.1, 0.3]
    sigma : float, default=4
        amount of gaussian blur [2, 8]
    gamma : float, default=1
        correct contrast (<1 : put more white, =1 : no effect, >1 : put more black)
    upDir : int, default=10
        the diffusion tensor is updated every upDiter iteration (>0)
    nbIter : int, default=50
        number of iterations

    Returns
    -------
    timagetk.SpatialImage
        Output of the anisotropic filter.

    Notes
    -----
    The input image should be either of type 'uint8' or 'uint16'.

    As the ``ani3D`` executable works with 'uint8' arrays, we will convert 'uint16' images prior to calling it.

    """
    if image.dtype == "uint16":
        image = SpatialImage(np.round(image / 256), voxelsize=image.voxelsize, dtype='uint8')

    with TemporaryDirectory(prefix="ani3d_plugin_") as directory:
        input_image_name = "ani_input.inr"
        imsave(path.join(directory, input_image_name), image)
        completed = subprocess.run(
            ["ani3D", path.join(directory, input_image_name), str(K), str(sigma), str(gamma), str(upDir), str(nbIter)],
            cwd=directory,
            capture_output=True,
            text=True,
            check=True,
        )

        output_loc = None
        for line in completed.stdout.splitlines():
            if line.startswith("image : "):
                logging.debug(line)
                output_loc = line.split()[-1]
                logging.debug("  ->  " + str(output_loc))

        output_image = imread(path.join(directory, output_loc))
    return output_image
