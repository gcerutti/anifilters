from time import time as current_time

import numpy as np

from timagetk import SpatialImage

from .operators import diffusion_tensor, evolution
from .operators import diffusion_tensor_numpy, evolution_numpy


default_iterations = 50
default_update = 10
default_dt = 0.1


def ani3D(img, K=0.2, sigma=3., gamma=1, update_iterations=default_update, iterations=default_iterations, filter_type='h', dt=default_dt):
    start_time = current_time()

    ani_img = img.get_array().astype(float)
    if gamma != 1:
        ani_img = np.power(ani_img / 255., gamma) * 255

    updates = int(np.floor(iterations/update_iterations))
    progress = ''
    for i in range(iterations + 1):
        if i % update_iterations == 0 and i<iterations:
            progress += '|'
            sys.stdout.write('\r')
            sys.stdout.write("[{}{}] {}% ({} diffusion tensor...)".format(progress,
                                                                          ' ' * (iterations + updates - i),
                                                                          int(100 * i / iterations),
                                                                          'computing' if i==0 else 'updating'))
            sys.stdout.flush()
            updates -= 1

            D = diffusion_tensor(ani_img, K, sigma, filter_type)
            sigma = sigma / 1.5

        progress += '='
        sys.stdout.write('\r')
        sys.stdout.write("[{}{}] {}%                                ".format(progress, ' '*(iterations+updates-i), int(100*i/iterations)))
        sys.stdout.flush()

        ani_img += evolution(ani_img, D, backward=i%2==0) * dt
        ani_img = np.maximum(0, np.minimum(255, ani_img))

    sys.stdout.write('\n')
    sys.stdout.flush()
    print("done in {}s".format(np.round(current_time() - start_time,2)))

    ani_img = SpatialImage(np.round(ani_img).astype(img.dtype), voxelsize=img.voxelsize)

    return ani_img


def ani3D_numpy(img, K=0.2, sigma=3., gamma=1, update_iterations=default_update, iterations=default_iterations, dt=default_dt):
    start_time = current_time()

    img_array = img.get_array().astype(float)
    if gamma != 1:
        img_array = np.power(img_array / 255., gamma) * 255

    updates = int(np.floor(iterations/update_iterations))
    progress = ''
    for i in range(iterations + 1):

        if i % update_iterations == 0 and i<iterations:
            progress += '|'
            sys.stdout.write('\r')
            sys.stdout.write("[{}{}] {}% ({} diffusion tensor...)".format(progress,
                                                                          ' ' * (iterations + updates - i),
                                                                          int(100 * i / iterations),
                                                                          'computing' if i==0 else 'updating'))
            sys.stdout.flush()
            updates -= 1

            D = diffusion_tensor_numpy(img_array, K, sigma)
            sigma = sigma / 1.5

        progress += '='
        sys.stdout.write('\r')
        sys.stdout.write("[{}{}] {}%                                ".format(progress, ' '*(iterations+updates-i), int(100*i/iterations)))
        sys.stdout.flush()

        img_array += evolution_numpy(img_array, D) * dt
        img_array = np.maximum(0, np.minimum(255, img_array))

    sys.stdout.write('\n')
    sys.stdout.flush()
    print("done in {}s".format(np.round(current_time() - start_time,2)))

    ani_img = SpatialImage(np.round(img_array).astype(img.dtype), voxelsize=img.voxelsize)

    return ani_img


if __name__ == "__main__":
    from time import time as current_time

    import numpy as np

    from timagetk.io import imread, imsave

    image_filename = "/data/t3_cut.inr.gz"

    img = imread(image_filename)

    ani_img_numpy = ani3D_numpy(img)
    ani_img = ani3D(img)
    #imsave("/Users/gcerutti/Development/rdp/anifilters/data/t3_cut_ani.inr.gz",ani_img)
    #imsave("/Users/gcerutti/Development/rdp/anifilters/data/t3_cut_ani_np.inr.gz",ani_img_numpy)

    import matplotlib.pyplot as plt
    figure = plt.figure(0)
    figure.clf()

    figure.add_subplot(1, 3, 1)
    figure.gca().imshow(ani_img[:, :, ani_img.shape[2]//2],cmap='gray',vmin=0,vmax=255)

    figure.add_subplot(1, 3, 2)
    figure.gca().imshow(ani_img_numpy[:, :, ani_img.shape[2]//2], cmap='gray', vmin=0, vmax=255)

    error = np.abs(ani_img.get_array().astype(float) - ani_img_numpy.get_array().astype(float))

    figure.add_subplot(1, 3, 3)
    figure.gca().imshow(error[:, :, ani_img.shape[2]//2],cmap='jet',vmin=0,vmax=64)

    figure.show()