from time import time as current_time
from multiprocessing.pool import Pool

import numpy as np
import scipy.ndimage as nd

from skimage.feature.corner import hessian_matrix


def div(vector_field, backward=True):
    divergence = np.zeros_like(vector_field).astype(float)

    if backward:
        divergence[1:, :, :, 0] = vector_field[1:, :, :, 0] - vector_field[:-1, :, :, 0]
        divergence[0, :, :, 0] = divergence[1, :, :, 0]
        divergence[:, 1:, :, 1] = vector_field[:, 1:, :, 1] - vector_field[:, :-1, :, 1]
        divergence[:, 0, :, 1] = divergence[:, 1, :, 1]
        divergence[:, :, 1:, 2] = vector_field[:, :, 1:, 2] - vector_field[:, :, :-1, 2]
        divergence[:, :, 0, 2] = divergence[:, :, 1, 2]
    else:  # forward
        divergence[:-1, :, :, 0] = vector_field[1:, :, :, 0] - vector_field[:-1, :, :, 0]
        divergence[-1, :, :, 0] = divergence[-2, :, :, 0]
        divergence[:, :-1, :, 1] = vector_field[:, 1:, :, 1] - vector_field[:, :-1, :, 1]
        divergence[:, -1, :, 1] = divergence[:, -2, :, 1]
        divergence[:, :, :-1, 2] = vector_field[:, :, 1:, 2] - vector_field[:, :, :-1, 2]
        divergence[:, :, -1, 2] = divergence[:, :, -2, 2]

    return divergence.sum(axis=-1)


def div_numpy(vector_field):
    derivatives = [np.gradient(vector_field[:,:,:,i], axis=i) for i in range(3)]
    return np.ufunc.reduce(np.add, derivatives)


def gradient(img_array):
    grad = np.zeros(img_array.shape + (3,), float)

    grad[1:-1, :, :, 0] = img_array[2:, :, :] - img_array[:-2, :, :]
    grad[:, 1:-1, :, 1] = img_array[:, 2:, :] - img_array[:, :-2, :]
    grad[:, :, 1:-1, 2] = img_array[:, :, 2:] - img_array[:, :, :-2]

    return grad



def hessian(img_array):
    hess = np.zeros(img_array.shape + (3, 3,), float)

    hess[1:-1, :, :, 0, 0] = img_array[2:, :, :] + img_array[:-2, :, :] - 2 * img_array[1:-1, :, :]
    hess[:, 1:-1, :, 1, 1] = img_array[:, 2:, :] + img_array[:, :-2, :] - 2 * img_array[:, 1:-1, :]
    hess[:, :, 1:-1, 2, 2] = img_array[:, :, 2:] + img_array[:, :, :-2] - 2 * img_array[:, :, 1:-1]

    hess[1:-1, 1:-1, :, 0, 1] = (img_array[2:, 2:, :] + img_array[:-2, :-2, :] - img_array[2:, :-2, :] - img_array[:-2, 2:, :]) / 4.
    hess[1:-1, 1:-1, :, 1, 0] = hess[1:-1, 1:-1, :, 0, 1]
    hess[1:-1, :, 1:-1, 0, 2] = (img_array[2:, :, 2:] + img_array[:-2, :, :-2] - img_array[2:, :, :-2] - img_array[:-2, :, 2:]) / 4.
    hess[1:-1, :, 1:-1, 2, 0] = hess[1:-1, :, 1:-1, 0, 2]
    hess[:, 1:-1, 1:-1, 1, 2] = (img_array[:, 2:, 2:] + img_array[:, :-2, :-2] - img_array[:, 2:, :-2] - img_array[:, :-2, 2:]) / 4.
    hess[:, 1:-1, 1:-1, 2, 1] = hess[:, 1:-1, 1:-1, 1, 2]

    return hess


def gradient_numpy(img_array):
    return np.transpose(np.gradient(img_array),(1,2,3,0))


def hessian_numpy(img_array):
    gradient = np.gradient(img_array)
    hess = np.transpose(np.gradient(gradient,axis=(1,2,3)),(2,3,4,0,1))
    return hess


def hessian_gaussian(img_array, sigma):
    gradients = [nd.gaussian_filter1d(img_array, sigma, axis=i, order=1) for i in range(img_array.ndim)]
    hess = [[nd.gaussian_filter1d(g, sigma, axis=j, order=1) for j in range(img_array.ndim)] for g in gradients]
    hess = np.transpose(hess,(2, 3, 4, 0, 1))
    return hess


def hessian_skimage(img_array, sigma):
    hess_values = hessian_matrix(img_array, sigma)

    def _hessian_matrix_image(H_elems):
        from itertools import combinations_with_replacement
        image = H_elems[0]
        hessian_image = np.zeros(image.shape + (image.ndim, image.ndim))
        for idx, (row, col) in enumerate(combinations_with_replacement(range(image.ndim), 2)):
            hessian_image[..., row, col] = H_elems[idx]
            hessian_image[..., col, row] = H_elems[idx]
        return hessian_image

    hess = _hessian_matrix_image(hess_values)
    return hess


def diffusion_tensor_grad(img_array, K, sigma):
    img_array_blur = nd.gaussian_filter(img_array, sigma)
    grad = gradient(img_array_blur)

    D = grad[:, :, :, np.newaxis, :] * grad[:, :, :, :, np.newaxis]

    eigen = Pool().map(np.linalg.eigh, D)
    val = np.array([e[0] for e in eigen])
    vec = np.array([e[1] for e in eigen])

    dlambda = np.exp(-np.power(val[:, :, :, 0] / K, 2))

    diffusion_tensor = dlambda[:, :, :, np.newaxis, np.newaxis] * (vec[:, :, :, :, np.newaxis, :] * vec[:, :, :, :, :, np.newaxis]).sum(axis=-3)

    return diffusion_tensor


def diffusion_tensor_hess(img_array, K, sigma):
    img_array_blur = nd.gaussian_filter(img_array, sigma)
    hess = hessian(img_array_blur)

    eigen = Pool().map(np.linalg.eigh, hess)
    val = np.array([e[0] for e in eigen])
    vec = np.array([e[1] for e in eigen])

    dlambda = np.exp(-np.power(val / K, 2))
    dlambda[val > 0] = 1.

    diffusion_tensor = (dlambda[:, :, :, :, np.newaxis, np.newaxis] * vec[:, :, :, :, np.newaxis, :] * vec[:, :, :, :, :, np.newaxis]).sum(axis=-3)

    return diffusion_tensor


def _diffusion_tensor(H, K=0.2):
    val, vec = np.linalg.eigh(H)

    dlambda = np.exp(-np.power(val / K, 2))
    dlambda[val > 0] = 1.

    return (dlambda[:, :, :, np.newaxis, np.newaxis] * vec[:, :, :, np.newaxis, :] * vec[:, :, :, :, np.newaxis]).sum(axis=-3)


def diffusion_tensor_numpy(img_array, K, sigma, use_skimage=True):
    # start_time = current_time()
    if use_skimage:
        hess = hessian_skimage(img_array, sigma)
    else:
        hess = hessian_gaussian(img_array, sigma)
    # print("\n")
    # print("hessian computed in {}s".format(np.round(current_time() - start_time,2)))

    # start_time = current_time()

    eigen = Pool().map(np.linalg.eigh, hess)
    val = np.array([e[0] for e in eigen])
    vec = np.array([e[1] for e in eigen])

    dlambda = np.exp(-np.power(val / K, 2))
    dlambda[val > 0] = 1.

    diffusion_tensor = (dlambda[:, :, :, :, np.newaxis, np.newaxis] * vec[:, :, :, :, np.newaxis, :] * vec[:, :, :, :, :, np.newaxis]).sum(axis=-3)

    # diffusion_tensor = np.array(Pool().map(lambda H : _diffusion_tensor(H, K=K), hess))
    # print("\n")
    # print("diffusion tensor computed in {}s".format(np.round(current_time() - start_time,2)))

    return diffusion_tensor


def diffusion_tensor(img_array, K, sigma, filter_type='h'):
    if filter_type == 'g':
        return diffusion_tensor_grad(img_array, K, sigma)
    else:
        return diffusion_tensor_hess(img_array, K, sigma)


def evolution(img_array, D, backward=True):
    grad = np.zeros(img_array.shape + (3,), float)
    if backward:
        grad[:-1, :, :, 0] = img_array[1:, :, :] - img_array[:-1, :, :]
        grad[:, :-1, :, 1] = img_array[:, 1:, :] - img_array[:, :-1, :]
        grad[:, :, :-1, 2] = img_array[:, :, 1:] - img_array[:, :, :-1]
    else:  # forward
        grad[1:, :, :, 0] = img_array[1:, :, :] - img_array[:-1, :, :]
        grad[:, 1:, :, 1] = img_array[:, 1:, :] - img_array[:, :-1, :]
        grad[:, :, 1:, 2] = img_array[:, :, 1:] - img_array[:, :, :-1]

    vector_field = (D * grad[:,:,:,np.newaxis]).sum(axis=-1)

    return div(vector_field, backward)


def evolution_numpy(img_array, D):
    grad = gradient_numpy(img_array)
    vector_field = (D * grad[:,:,:,np.newaxis]).sum(axis=-1)

    return div_numpy(vector_field)