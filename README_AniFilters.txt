----
# AniFilters
## Anisotropic filters which reinforce line-structures in 2D and planar structures in 3D

----

#### Authors :
Typhaine Moreau, Annamaria Kiss (Laboratoire RDP, ENS Lyon)

----
#### Implementation based on :
Schmidt, T., Pasternak, T., Liu, K., Blein, T., Aubry-Hivet, D., Dovzhenko, A., Duerr, J., Teale, W., Ditengou, F. A., Burkhardt, H., Ronneberger, O. and Palme, K. (2014), **The iRoCS Toolbox – 3D analysis of the plant root apical meristem at cellular resolution**. *The Plant Journal*, 77: 806–814. doi: 10.1111/tpj.12429

----

### Download

----

```bash
svn --username $USER checkout http://forge.cbp.ens-lyon.fr/svn/anifilters
```

### Compile

----

```bash
cd anifilters
./anifilters_compile.sh
```

### The following binaries will be generated in the bin directory :

----

* `ani2D` --> anisotropic filter reinforcing linear structures
* `ani3D` --> anisotropic filter reinforcing planar structures (.inr, .inr.gz files)
* `ani3D-tiff` --> same as ani3D for tiff files


### Usage (the same syntax for the three versions)

----

```bash
ani2D image K sigma gamma upDiter nbIter
ani3D image K sigma gamma upDiter nbIter
ani3D-tiff image K sigma gamma upDiter nbIter
```

* `image` : grayscale image .jpg, .png (2D version), .inr or .inr.gz (3D version) or .tif (3D-tiff version)
* `K` : proportionnal to the cell walls width [0.1 - 0.3]
* `sigma` : amount of gaussian blur [2 - 8]
* `gamma` : correct contrast ( <1:put more white / 1: no effect / >1:put more black)
* `upDiter` : the diffusion tensor is updated every upDiter iteration
* `nbIter` : number of iterations


### Test

------

```bash
ani3D t3_cut.inr.gz 0.2 3 1 10 50
```


