/*
Anisotropic blur : removes noise while keeping cell walls
Parallel version
3D only

To compile :
 g++ -o ani3D.exe ani3D.cpp -O2 -L/usr/X11R6/lib -lm -lpthread -lX11 -fopenmp -l:libtiff.so.4
 Needs CImg.h

To execute :
 ./ani3D img K sigma gamma upDiter nbIter [filtertype]

 img : image in .inr or .inr.gz
 K : proportional to width of cell walls (0.1 - 0.3)
 sigma : amount of gaussian blur (float)
 gamma : contrast correction (0.3 - 1.2 / 1=no effect)
 upDiter : recompute Diffusion Tensor every upDiter (int)
 nbIter : number of iteration
 filtertype (optional) : by default hessian diffusion tensor, if "g" marked then gradient based diffusion tensor
 
 * authors : Typhaine Moreau, Annamaria Kiss (RDP)
 * ENS-Lyon
 * 
 * Reference : Supplementary of Schmidt et al. The irocs toolbox – 3d analysis
 * of the plant root apical meristem at cellular resolution
 * The Plant Journal, page 806–814, 2014
 
*/
#include <iostream>
#include <math.h>
#include <sstream>
#include <fstream>

#include "CImg.h"
#include <omp.h>

using namespace cimg_library;
using namespace std;

#include "cimg_ani_lib.cpp"

//-----------------------------------------------------------------------------
//Main
//-----------------------------------------------------------------------------
int main (int argc, char* argv[])
{
  double begin_main=omp_get_wtime();

  if(argc<7 || argc>8)
    {
      cout<<"!! wrong number of arguments"<<endl;
      cout<<"how to execute : ./ani3D img K sigma gamma upDiter nbIter [type]"<<endl;
      cout<<" img : image in .inr or .inr.gz"<<endl;
	  cout<<" K : proportional to width of cell walls (0.1 - 0.3)"<<endl;
	  cout<<" sigma : amount of gaussian blur (float)"<<endl;
	  cout<<" gamma : contrast correction (0.3 - 1.2 / 1=no effect)"<<endl;
	  cout<<" upDiter : recompute Diffusion Tensor every upDiter (int)"<<endl;
	  cout<<" nbIter : number of iteration"<<endl;
	  cout<<" filtertype (optional) : by default hessian diffusion tensor, if 'g' marked then gradient based diffusion tensor"<<endl;
 
      return 0;
    }


  string filter_type="h";
  if (argc==8)
	{
	if (string(argv[7]).compare("g")==0)
		{filter_type="g";}
	}
	
  
  //Open image with name in argument
  //image is converted to unsigned char (0 to 255) and then to float
  string name=argv[1];
  CImg<> img1;
  CImg<char> description;
  float tailleVoxel[3] = {0}; // resolution initialisation
  bool gzipped = false;
  bool tiffile = false;
  
  if(name.compare(name.size()-4,4,".inr")==0)
    {   
      img1.load(name.c_str());
      img1.get_load_inr(name.c_str(),tailleVoxel); // reads resolution
    }
  else if(name.compare(name.size()-7,7,".inr.gz")==0)
    {
      gzipped = true;
      string oldname = name;
      name.erase(name.size()-3);
      string zip="gunzip -c "+oldname+" > "+name;
      if(system(zip.c_str())); // decompress image file
      img1.load(name.c_str()); //read image
      img1.get_load_inr(name.c_str(),tailleVoxel); // read resolution
      zip="rm "+name; 
      if(system(zip.c_str())); //removes decompressed image    
    }
  else if(name.compare(name.size()-4,4,".tif")==0)
	{
	  tiffile = true;
	  cout<<"file to read : "<<name<<endl;
	  img1.load_tiff(name.c_str(),0,~0U,1,tailleVoxel,&description);
	}
  else
    {cout<<"!! wrong file extension (not an inr/inr.gz/tif)  : "<<name<<endl;
      return 0;}
 
  CImg<float> img=img1;
  img1.assign();
  cout<<"image : "<<name<<endl;
  cout<<"gzipped : "<<gzipped<<endl;
  cout<<"image depth : "<<img.depth()<<endl;
  if(img.depth()<=1)
    {
      cout<<"This is not a 3D image"<<endl;
      return  0;
    }
  //---------------------------------------------------------Parameters
  float dt=0.1;
  float  K= atof(argv[2]);
  float sigma=atof(argv[3]);
  float gamma=atof(argv[4]);
  int upDIter=atoi(argv[5]);
  int nbIter=atoi(argv[6]);

  cout<<"Voxel size : ("<<tailleVoxel[0]<<","<<tailleVoxel[1]<<","<<tailleVoxel[2]<<")"<<endl;
  
  //--------------------------------------------------------Name and directories
  ostringstream oss,oss2;
  oss << K*10;
  string ar2=oss.str();
  oss2 << gamma*10;
  string ar4=oss2.str();
  string insert=string("_ani-K")+ar2+"s"+argv[3]+"g"+ar4+"itD"+argv[5]+"-type-"+filter_type;
  name.insert(name.size()-4,insert);
  size_t test=name.rfind("/");
  if(test!=name.npos)
    {name.erase(0,test+1);}
  string nameDir=name;
  nameDir.erase(name.size()-4,4);
  string directory="mkdir -p "+nameDir;
  cout<<directory<<endl;
  if(system(directory.c_str())); 
  name=nameDir+"/"+name;

  ofstream file;
  string nameLog=name;
  nameLog.erase(name.size()-4);
  nameLog+=string("it")+argv[6]+string("LOG.txt");

  file.open(nameLog.c_str());
  file<<argv[0]<<endl;
  time_t t;
  struct tm * timeinfo;
  time(&t);
  timeinfo=localtime(&t);
  file<<asctime(timeinfo);
  file<<"image : "<<argv[1]<<endl;
  file<<"_________________________________"<<endl;
  file<<"Parameters"<<endl;
  file<<"time step \t \t \t dt=0.1"<<endl;
  file<<"diffusion tensor parameter \t K = "<<argv[2]<<endl;
  file<<"gaussian blur \t \t \t sigma = "<<argv[3]<<endl;
  file<<"gamma correction \t \t gamma = "<<argv[4]<<endl;
  file<<"update D every "<<argv[5]<<" iterations"<<endl;
  file<<"\n maximum number of iterations = "<<argv[6]<<endl;
  file<<"\n";

  //---------------------------------------------------------------------Processing
  //Gamma correction (contrast)  
  if (gamma!=1)
    {img=(img/255).pow(gamma)*=255;}

  //Evolution during nbIter
      //-------------------------------------------------------------------------3D
      cout<<"3D image"<<endl;
      cout <<"Computing diffusion tensor"<<endl;

      //Compute diffusion tensor D
      int nthreads;
      CImgList<float> D;
      double begin=omp_get_wtime();
      D=diffusion_tensor(img,K,sigma,&nthreads, filter_type);
      ostringstream ossN;
      ossN<<nthreads;
      file<<"number of threads : "<<ossN.str()<<endl;
      double end=omp_get_wtime();
      double time_compute=double(end-begin);
      file <<"time to compute : "<<time_compute<<" sec\n"<<endl;

      for(int i=0;i<nbIter;i++)
	{
	  //Every upDIter time step, update diffusion tensor D
	  if ((i%upDIter==0) and (i!=0))
	    {
	      cout <<"update diffusion tensor"<<endl;
	      sigma=sigma/1.5;
	      file <<"new sigma : "<<sigma<<endl;
	      double begin=omp_get_wtime();
	      D.assign();
	      D=diffusion_tensor(img,K,sigma,&nthreads, filter_type);
	      double end=omp_get_wtime();
	      double time_compute=double(end-begin);
	      file <<"time to update : "<<time_compute<<" sec\n"<<endl;
	    }

	  //Evolution
	  img=img+evolution(img,D,i)*dt;  

	  //Reset pixel<0 or pixel>255
	  cimg_forXYZ(img,x,y,z)
	    {
	      if(img(x,y,z)<0) {img(x,y,z)=0;}
	      else if(img(x,y,z)>255) {img(x,y,z)=255;}
	    }
	  cout<<i<<"/"<<nbIter-1<<endl;

	  //Saving results
	  if(((i%10==0)and(i!=0))or(i==nbIter-1))
	    {
	      string newname=name;
	      ostringstream oss3;
	      oss3 << i;
	      string insert="_it"+oss3.str();
	      newname.insert(name.size()-4,insert);
	      CImg<unsigned char> imgbis=img;
	      file<<"image save at t="<<i<<"\n"<<endl;
	      cout<<"Save image"<<endl;
	      cout<<"image : "<<newname<<endl;
	      if (tiffile)
			{
				imgbis.save_tiff(newname.c_str(),0,tailleVoxel,description.data());
			}
		  else
			{
			  imgbis.save_inr(newname.c_str(),tailleVoxel);
			  if (gzipped) 
			  {
				string zip="gzip -f "+newname;
				if(system(zip.c_str()));
				}
		  }
	      imgbis.assign();
	    }
	}

  double end_main=omp_get_wtime();
  double time=double(end_main-begin_main);
  cout <<"time elapsed : "<<time<<" seconds"<<endl;
  float minutes=time/60;
  cout << "  = ~ "<<minutes<<" minutes"<<endl;

  //Log file
  file <<"\ntime elapsed : "<<time<<" seconds"<<endl;
  file << "  = ~ "<<minutes<<" minutes"<<endl;

  file <<" width : "<<img.width()<<endl;
  file <<" height : "<<img.height()<<endl;
  file <<" depth : "<<img.depth()<<endl;
  file <<" number of pixel : "<<img.width()*img.height()*img.depth()<<endl;

  file.close();

  return 0;
}
