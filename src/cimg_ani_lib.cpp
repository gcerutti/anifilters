
//Div operator on a 3D vector field V(Vx,Vy,Vz)
//------------------------------------------------------------------------------------
CImg<float> div (const CImgList<float> & vectorField, int i)
{  
  CImg<float> divergence(vectorField[0]._width,vectorField[0]._height,vectorField[0]._depth,1,0);
  //Image Loop - compute alternatively backward and forward finite differences
  cimg_forXYZ(vectorField[0],x,y,z)
    {
	float divX=0;
	float divY=0;
	float divZ=0;

	if(i%2==0)
	  {
	  //backward
 	  if(x==0)
	  {divX=(vectorField[0](x+1,y,z)-vectorField[0](x,y,z));}  //border dVx/dx
	  else
          {divX=(vectorField[0](x,y,z)-vectorField[0](x-1,y,z));}  //  dVx/dx
	  if(y==0)
	  {divY=(vectorField[1](x,y+1,z)-vectorField[1](x,y,z));}  //border dVy/dy
	  else
	  {divY=(vectorField[1](x,y,z)-vectorField[1](x,y-1,z));}  //  dVy/dy
	  if(z==0)
	  {divZ=(vectorField[2](x,y,z+1)-vectorField[2](x,y,z));}  //border dVz/dz
	  else
	  {divZ=(vectorField[2](x,y,z)-vectorField[2](x,y,z-1));}  //  dVz/dz
	  }
	else
	  {
	  //forward
	  if(x==vectorField[0]._width-1)
	  {divX=(vectorField[0](x,y,z)-vectorField[0](x-1,y,z));}  //border dVx/dx
	  else
          {divX=(vectorField[0](x+1,y,z)-vectorField[0](x,y,z));}  //  dVx/dx
	  if(y==vectorField[1]._height-1)
	  {divY=(vectorField[1](x,y,z)-vectorField[1](x,y-1,z));}  //border dVy/dy
	  else
	  {divY=(vectorField[1](x,y+1,z)-vectorField[1](x,y,z));}  //  dVy/dy
	  if(z==vectorField[2]._depth-1)
	  {divZ=(vectorField[2](x,y,z)-vectorField[2](x,y,z-1));}  //border dVz/dz
	  else
	  {divZ=(vectorField[2](x,y,z+1)-vectorField[2](x,y,z));}  //  dVz/dz
	  }

	divergence(x,y,z)=divX+divY+divZ;
    }
  return divergence;
}


//Gradient 3D with central finite differences
//----------------------------------------------------------------------------
CImgList<float> gradient(CImg<float> const & img)
{
  //List of 3 images the same size as img
  CImgList<float> grad(3,img._width,img._height,img._depth,1,0);

  //Image loop with a 3*3*3 neighborhood
  CImg_3x3x3(I,float);
  cimg_for3x3x3(img,x,y,z,0,I,float)
    {
      grad(0,x,y,z) = (Incc - Ipcc)/2; //grad x = (img(x+1,y,z)-img(x-1,y,z))/2
      grad(1,x,y,z) = (Icnc - Icpc)/2; //grad y
      grad(2,x,y,z) = (Iccn - Iccp)/2; //grad z
    }
  return grad;
}


//Weickert diffusion tensor based on gradient 3D
//---------------------------------------------------------------------------------
CImgList<float> diffusion_tensor_grad(const CImg<float> & img, float K,float sigma, int *nthreads)
{
  //List of 5 images the same size as img
  //The diffusion tensor is a 3*3 symmetric matrix -> only 6 differents values 
  
  CImgList<float> diffusionTensor(6,img._width,img._height,img._depth,1,0);
  CImg<float> imgBlur=img.get_blur(sigma); 
  CImgList<float> grad = gradient(imgBlur);

#pragma omp parallel shared(imgBlur,diffusionTensor)
  {
#pragma omp for schedule(dynamic)
  for(int z=0;z<img._depth;z++)
      {for(int y=0;y<img._height;y++)
	  {for(int x=0;x<img._width;x++)
      { 
		
	CImg<float> D(3,3,1,1,0);
	D(0,0) = grad(0,x,y,z)*grad(0,x,y,z);          // Hxx
	D(1,1) = grad(1,x,y,z)*grad(1,x,y,z);          // Hyy
	D(2,2) = grad(2,x,y,z)*grad(2,x,y,z);          // Hzz
	D(1,0) = D(0,1) = grad(1,x,y,z)*grad(0,x,y,z); // Hxy = Hyx
	D(2,0) = D(0,2) = grad(2,x,y,z)*grad(0,x,y,z); // Hxz = Hzx
	D(2,1) = D(1,2) = grad(2,x,y,z)*grad(1,x,y,z); // Hyz = Hzy
 
	//val : 3 eigen values as an array, decreasing order
	//vec : 3 eigen vectors as a 3*3 image
	//have to be double or create NaN
	CImg<double> val,vec;
	D.symmetric_eigen(val,vec);

	//Compute diffusion tensor D
	for(int i=0;i<3;i++)
	  {
	    float dlambda=1;
	    if(i==0)
	      {
		dlambda=exp(-pow(val[i]/K,2));
	      }
	    diffusionTensor(0,x,y,z)+=dlambda*vec(i,0)*vec(i,0); //Dxx
	    diffusionTensor(1,x,y,z)+=dlambda*vec(i,0)*vec(i,1); //Dxy = Dyx
	    diffusionTensor(2,x,y,z)+=dlambda*vec(i,0)*vec(i,2); //Dxz = Dzx
	    diffusionTensor(3,x,y,z)+=dlambda*vec(i,1)*vec(i,1); //Dyy
	    diffusionTensor(4,x,y,z)+=dlambda*vec(i,1)*vec(i,2); //Dyz = Dzy
	    diffusionTensor(5,x,y,z)+=dlambda*vec(i,2)*vec(i,2); //Dzz	    
	  }
	//     |D(0) D(1) D(2)|
	// D = |D(1) D(3) D(4)|
	//     |D(2) D(4) D(5)|     
      }}}
  //to write on log file the number of threads
  *nthreads = omp_get_num_threads();
  }
  return diffusionTensor;
}


//Schmidt diffusion tensor with hessian 3D
//---------------------------------------------------------------------------------
CImgList<float> diffusion_tensor_hess(const CImg<float> & img, float K,float sigma, int *nthreads)
{
  //List of 5 images the same size as img
  //The diffusion tensor is a 3*3 symmetric matrix -> only 6 differents values 
  CImgList<float> diffusionTensor(6,img._width,img._height,img._depth,1,0);
  CImg<float> imgBlur=img.get_blur(sigma);

#pragma omp parallel shared(imgBlur,diffusionTensor)
  {
#pragma omp for schedule(dynamic)
  for(int z=0;z<img._depth;z++)
      {for(int y=0;y<img._height;y++)
	  {for(int x=0;x<img._width;x++)
      { 
	int xp=x-1;
	if (x==0){xp=x;}
	int xn=x+1;
	if (x==imgBlur._width-1){xn=x;}
	int yp=y-1;
	if (y==0){yp=y;}
	int yn=y+1;
	if (y==imgBlur._height-1){yn=y;}
	int zp=z-1;
	if (z==0){zp=z;}
	int zn=z+1;
	if (z==imgBlur._depth-1){zn=z;}

	CImg<float> hess(3,3,1,1,0);
	hess(0,0) = imgBlur(xp,y,z) + imgBlur(xn,y,z) - 2*imgBlur(x,y,z);          // Hxx
	hess(1,1) = imgBlur(x,yp,z) + imgBlur(x,yn,z) - 2*imgBlur(x,y,z);          // Hyy
	hess(2,2) = imgBlur(x,y,zp) + imgBlur(x,y,zn) - 2*imgBlur(x,y,z);          // Hzz
	hess(1,0) = hess(0,1) = (imgBlur(xp,yp,z) + imgBlur(xn,yn,z) - imgBlur(xp,yn,z) - imgBlur(xn,yp,z))/4; // Hxy = Hyx
	hess(2,0) = hess(0,2) = (imgBlur(xp,y,zp) + imgBlur(xn,y,zn) - imgBlur(xp,y,zn) - imgBlur(xn,y,zp))/4; // Hxz = Hzx
	hess(2,1) = hess(1,2) = (imgBlur(x,yp,zp) + imgBlur(x,yn,zn) - imgBlur(x,yp,zn) - imgBlur(x,yn,zp))/4; // Hyz = Hzy
	
	//val : 3 eigen values as an array, decreasing order
	//vec : 3 eigen vectors as a 3*3 image
	//have to be double or create NaN
	CImg<double> val,vec;
	hess.symmetric_eigen(val,vec);

	//Compute diffusion tensor D
	for(int i=0;i<3;i++)
	  {
	    float dlambda=1;
	    if(val[i]<0)
	      {
		dlambda=exp(-pow(val[i]/K,2));
	      }
	    diffusionTensor(0,x,y,z)+=dlambda*vec(i,0)*vec(i,0); //Dxx
	    diffusionTensor(1,x,y,z)+=dlambda*vec(i,0)*vec(i,1); //Dxy = Dyx
	    diffusionTensor(2,x,y,z)+=dlambda*vec(i,0)*vec(i,2); //Dxz = Dzx
	    diffusionTensor(3,x,y,z)+=dlambda*vec(i,1)*vec(i,1); //Dyy
	    diffusionTensor(4,x,y,z)+=dlambda*vec(i,1)*vec(i,2); //Dyz = Dzy
	    diffusionTensor(5,x,y,z)+=dlambda*vec(i,2)*vec(i,2); //Dzz	    
	  }
	//     |D(0) D(1) D(2)|
	// D = |D(1) D(3) D(4)|
	//     |D(2) D(4) D(5)|     
      }}}
  //to write on log file the number of threads
  *nthreads = omp_get_num_threads();
  }
  return diffusionTensor;
}

//Evolution : Compute div(D*grad(I))  3D
//-----------------------------------------------------------------------
CImg<float> evolution(const CImg<float> & img,const CImgList<float> & D, int i)
{	     
  //list of 3 images the same size as img
  CImgList<float> vectorField(3,img._width,img._height,img._depth,1,0);

  //Image loop with a 3*3*3 neighborhood
  //compute grad and D*grad for every pixel
  //     |D(0) D(1) D(2)|         |x|
  // D = |D(1) D(3) D(4)|    grad=|y|
  //     |D(2) D(4) D(5)|         |z|

#pragma omp parallel shared(vectorField)
  {
#pragma omp for schedule(dynamic)
   for (int z = 0; z<img.depth(); z++) 
    {for (int y = 0; y<img.height(); y++) 
      {for (int x = 0; x<img.width(); x++)
      { 

	float gradx=0;
	float grady=0;
	float gradz=0;
	
	//compute grad
	if(i%2==0)
	  {
	    //Forward
	    if (x!=img._width-1) {gradx = img(x+1,y,z) - img(x,y,z);} 
	    if (y!=img._height-1) {grady = img(x,y+1,z) - img(x,y,z);} 
	    if (z!=img._depth-1) {gradz = img(x,y,z+1) - img(x,y,z);} 
	  }
	else
	  {
	    //Backward
	    if (x!=0) {gradx = img(x,y,z) - img(x-1,y,z);}  
	    if (y!=0) {grady = img(x,y,z) - img(x,y-1,z);} 
	    if (z!=0) {gradz = img(x,y,z) - img(x,y,z-1);}
	  }

	//compute D*grad
	vectorField(0,x,y,z)=D(0,x,y,z)*gradx + D(1,x,y,z)*grady + D(2,x,y,z)*gradz; //Vx
	vectorField(1,x,y,z)=D(1,x,y,z)*gradx + D(3,x,y,z)*grady + D(4,x,y,z)*gradz; //Vy
	vectorField(2,x,y,z)=D(2,x,y,z)*gradx + D(4,x,y,z)*grady + D(5,x,y,z)*gradz; //Vz
      }}}
  }
return div(vectorField,i);
}

CImgList<float> diffusion_tensor(const CImg<float> & img, float K,float sigma, int *nthreads, string filter_type)
{
	CImgList<float> diffusionTensor;
    if (filter_type.compare("g")==0)
		{
			diffusionTensor=diffusion_tensor_grad(img,K,sigma,nthreads);
		}
		else
		{
			diffusionTensor=diffusion_tensor_hess(img,K,sigma,nthreads);
		} 
return diffusionTensor;
}
