/*
Anisotropic blur : removes noise while keeping cell walls
Sequential version
2D only

To compile :
 g++ -o ani2D.exe anisotropic_blur_2D.cpp -O2 -L/usr/X11R6/lib -lm -lpthread -lX11
 Need CImg.h

To execute :
 ./ani2D img K sigma gamma upDiter nbIter

 img : image in common format (.png, .jpg ..)
 K : proportional to width of cell walls (0.1 - 0.3)
 sigma : amount of gaussian blur (float)
 gamma : contrast correction (0.3 - 1.2 / 1=no effect)
 upDiter : recompute Diffusion Tensor every upDiter (int)
 nbIter : number of iteration
*/

#include <iostream>
#include <math.h>
#include <sstream>
#include <ctime>
#include <fstream>

#include "CImg.h"

using namespace cimg_library;
using namespace std;

//Div operator on a 2D vector field
//------------------------------------------------------------------------------------
CImg<float> div2D (CImgList<float> vectorField,int i)
{  
  CImg<float> divergence(vectorField[0]._width,vectorField[0]._height,1,1,0);
  //Image Loop - compute alternatively forward and backward finite differences
  cimg_forXY(vectorField[0],x,y)
    {
	float divX=0;
	float divY=0;

	if(i%2==0)
	  {
	  //backward
 	  if(x==0)
	  {divX=(vectorField[0](x+1,y)-vectorField[0](x,y));}  //border dVx/dx
	  else
          {divX=(vectorField[0](x,y)-vectorField[0](x-1,y));}  //  dVx/dx
	  if(y==0)
	  {divY=(vectorField[1](x,y+1)-vectorField[1](x,y));}  //border dVy/dy
	  else
	  {divY=(vectorField[1](x,y)-vectorField[1](x,y-1));}  //  dVy/dy
	  }
	else
	  {
	  //forward
	  if(x==vectorField[0]._width)
	  {divX=(vectorField[0](x,y)-vectorField[0](x-1,y));}  //border dVx/dx
	  else
          {divX=(vectorField[0](x+1,y)-vectorField[0](x,y));}  //  dVx/dx
	  if(y==vectorField[1]._height)
	  {divY=(vectorField[1](x,y)-vectorField[1](x,y-1));}  //border dVy/dy
	  else
	  {divY=(vectorField[1](x,y+1)-vectorField[1](x,y));}  //  dVy/dy
	  }
	
	divergence(x,y)=divX+divY;
    }
  return divergence;
}

//Gradient 2D
//---------------------------------------------------------------------------------
CImgList<float> gradient2D(CImg<float> img, int i)
{
  //List of 2 images the same size as img
  CImgList<float> grad(2,img._width,img._height,1,1,0);

  //Image loop with a 3*3 neighborhood
  CImg_3x3(I,float);
  cimg_for3x3(img,x,y,0,0,I,float)
    {

      if(i%2==0)
	{
	//Forward
        grad(0,x,y) = (Inc - Icc); //grad x 
        grad(1,x,y) = (Icn - Icc); //grad y
	}
      else
        {
	//Backward
        grad(0,x,y) = (Icc - Ipc); //grad x 
        grad(1,x,y) = (Icc - Icp); //grad y
	}
   }
  return grad;
}

//Schmidt diffusion tensor with hessian 2D
//---------------------------------------------------------------------------------
CImgList<float> diffusion_tensor2D(CImg<float> img, float K, float sigma)
{
  //List of 3 images the same size as img
  //The diffusion tensor is a 2*2 symmetric matrix -> only 3 differents values 
  CImgList<float> diffusionTensor(3,img._width,img._height,1,1,0);
  CImg<float> imgBlur=img.get_blur(sigma);
  CImg<float> hess(2,2);

  //Image loop with a 3*3 neighborhood
  CImg_3x3(I,float);
  cimg_for3x3(imgBlur,x,y,0,0,I,float)
    { 
      //Hessian with finite difference
      hess(0,0) = Ipc + Inc - 2*Icc;          // Hxx
      hess(1,1) = Icp + Icn - 2*Icc;          // Hyy
      hess(1,0) = hess(0,1) = (Ipp + Inn - Ipn - Inp)/4; // Hxy = Hyx

      //val : 2 eigen values as an array, decreasing order
      //vec : 2 eigen vectors as a 2*2 image
      CImg<float> val,vec;
      hess.symmetric_eigen(val,vec);

      //Compute diffusion tensor D
      for(int i=0;i<2;i++)
	{
	  float dlambda=1;
	  if(val[i]<0)
	    {
	      dlambda=exp(-pow(val[i]/K,2));
	    }
	  diffusionTensor(0,x,y)+=dlambda*vec(i,0)*vec(i,0); //Dxx
	  diffusionTensor(1,x,y)+=dlambda*vec(i,0)*vec(i,1); //Dxy = Dyx
	  diffusionTensor(2,x,y)+=dlambda*vec(i,1)*vec(i,1); //Dyy
	}
      // D = |D(0) D(1)|  
      //     |D(1) D(2)|
    }
  return diffusionTensor;
}

//Evolution 2D : Compute div(D*grad(I))  2D
//-----------------------------------------------------------------------
CImg<float> evolution2D(CImg<float> img, CImgList<float> D, int i)
{
  CImgList<float> grad=gradient2D(img,i);
  CImgList<float> vectorField(2,grad[0]._width,grad[0]._height,grad[0]._depth);

  //D*grad for every pixel
  // D = |D(0) D(1)|    grad = |0|
  //     |D(1) D(2)|           |1|
  cimg_forXY(img,x,y)
    {
      vectorField(0,x,y)=D(0,x,y)*grad(0,x,y) + D(1,x,y)*grad(1,x,y); //Vx
      vectorField(1,x,y)=D(1,x,y)*grad(0,x,y) + D(2,x,y)*grad(1,x,y); //Vy
    }	
  return div2D(vectorField,i);
}

//Main
//-----------------------------------------------------------------------
int main (int argc, char* argv[])
{
  clock_t begin=clock();

  if(argc!=7)
    {
      cout<<"!! wrong number of arguments"<<endl;
      cout<<"how to execute : ./ani.exe img K sigma gamma upDiter nbIter"<<endl;
      return 0;
    }

  //Open image with name in argument
  //image is converted to unsigned char (0 to 255) and then to float
  string name=argv[1];
  CImg<unsigned char> imgPrevious(name.c_str());
  CImg<float> img=imgPrevious;
  imgPrevious.assign();
  cout<<"image : "<< name <<endl;
  if(img.depth()>1)
    {
      cout<<"This is not a 2D image"<<endl;
      return 0;
    }

  //---------------------------------------------------------Parameters
  float dt=0.1;
  float  K= atof(argv[2]);
  float sigma=atof(argv[3]);
  float gamma=atof(argv[4]);
  int upDIter=atoi(argv[5]);
  int nbIter=atoi(argv[6]);

 //--------------------------------------------------------Name and directories
  ostringstream oss,oss2;
  oss << K*10;
  string ar2=oss.str();
  oss2 << gamma*10;
  string ar4=oss2.str();
  string insert=string("_ani-K")+ar2+"s"+argv[3]+"g"+ar4+"itD"+argv[5];
  name.insert(name.size()-4,insert);
  size_t test=name.rfind("/");
  if(test!=name.npos)
    {name.erase(0,test+1);}
  string nameDir=name;
  nameDir.erase(name.size()-4,4);
  string directory="mkdir -p "+nameDir;
  cout<<directory<<endl;
  if(system(directory.c_str())); 
  name=nameDir+"/"+name;

  ofstream file;
  string nameLog=name;
  string nameImSave=name;
  nameLog.erase(name.size()-4);
  nameLog+=string("it")+argv[6]+string("LOG.txt");

  file.open(nameLog.c_str());
  file<<"image filename : "<<argv[1]<<endl;
  file<<"--------------------------------"<<endl;
  file<<"time step \t \t \t dt=0.1"<<endl;
  file<<"diffusion tensor parameter \t K = "<<argv[2]<<endl;
  file<<"gaussian blur \t \t \t sigma = "<<argv[3]<<endl;
  file<<"gamma correction \t \t gamma = "<<argv[4]<<endl;
  file<<"update D every "<<argv[5]<<" iterations"<<endl;
  file<<"\n maximum number of iterations = "<<argv[6]<<endl;
  file<<"\n";


  //---------------------------------------------------------------------Processing
  //Gamma correction (contrast)  
  if (gamma!=1)
    {img=(img/255).pow(gamma)*=255;}

  //Evolution during nbIter
  //--------------------------------------------------------------------------2D
  cout <<"Image 2D"<<endl;
  cout<<"Computing diffusion tensor"<<endl;
  CImgList<float> D=diffusion_tensor2D(img,K,sigma);

  for(int i=0;i<nbIter;i++)
    {
      if ((i%upDIter==0) and (i!=0))
	{
	  cout <<"update diffusion tensor"<<endl;
	  sigma=sigma/1.5;
	  D=diffusion_tensor2D(img,K,sigma);
	}
	    
      //Evolution
      img=img+evolution2D(img,D,i)*dt;     

      //Reset pixel<0 or pixel>255
      cimg_forXY(img,x,y)
	{
	  if(img(x,y)<0) {img(x,y)=0;}
	  else if(img(x,y)>255) {img(x,y)=255;}
	}
      cout<<i<<endl;
      
      // saving intermediate result every 20 iterations
      if ((i%20==0) and (i!=0))
	{	   
	   ostringstream oss;
		oss << i;
		string stri=oss.str();
	   string ar6=string("it")+stri;
	   	nameImSave = name ;
		nameImSave.insert(name.size()-4,ar6);
		cout <<"save image"<<endl;	 
		img.save(nameImSave.c_str());	  
	}

    }

  //Saving final result
  string ar6=string("it")+argv[6];
  name.insert(name.size()-4,ar6);
  img.save(name.c_str());	  
    
  clock_t end = clock();
  double time=double(end-begin)/CLOCKS_PER_SEC;
  cout <<"time elapsed : "<<time<<" seconds"<<endl;
  float minutes=time/60;
  cout << "  = ~ "<<minutes<<" minutes"<<endl;

  //Log file
  file <<"\ntime elapsed : "<<time<<" seconds"<<endl;
  file << "  = ~ "<<minutes<<" minutes"<<endl;
  file.close();

  return 0;
}
