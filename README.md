# AniFilters
## Anisotropic filters which reinforce line-structures in 2D and planar structures in 3D

----

#### Authors :
Typhaine Moreau, Annamaria Kiss (Laboratoire RDP, ENS Lyon)

----
#### Implementation based on :
Schmidt, T., Pasternak, T., Liu, K., Blein, T., Aubry-Hivet, D., Dovzhenko, A., Duerr, J., Teale, W., Ditengou, F. A., Burkhardt, H., Ronneberger, O. and Palme, K. (2014), **The iRoCS Toolbox – 3D analysis of the plant root apical meristem at cellular resolution**. *The Plant Journal*, 77: 806–814. doi: 10.1111/tpj.12429

----

### Download

----

```bash
git clone https://gitlab.inria.fr/gcerutti/anifilters.git
```

### Pre-requisite: install `conda`

----

If `conda` is not already installed (open a terminal window and type `conda` to check), you may choose to install either [the miniconda tool](https://docs.conda.io/en/latest/miniconda.html) or [the anaconda distribution](https://docs.anaconda.com/anaconda/install/) suitable for your OS.


### Install conda package

----

* Install the `anifilters` package in an existing conda environment:

```bash
conda install -c mosaic -c morpheme -c conda-forge anifilters
```

* Alternatively, you can create a new environment with the `anifilters` package:

```bash
conda create -n anifilters -c mosaic -c morpheme -c conda-forge anifilters
```

### Install from sources

----

```bash
cd anifilters
conda env create -f conda/env/anifilters.yaml
conda activate anifilters
python setup.py develop
```

### The following binaries will be installed in the environment:

----

* `ani2D` --> anisotropic filter reinforcing linear structures
* `ani3D` --> anisotropic filter reinforcing planar structures (.inr, .inr.gz files)
* `ani3D-tiff` --> same as ani3D for tiff files


### Usage (the same syntax for the three versions)

----

```bash
ani2D image K sigma gamma upDiter nbIter
ani3D image K sigma gamma upDiter nbIter
ani3D-tiff image K sigma gamma upDiter nbIter
```

* `image` : grayscale image .jpg, .png (2D version), .inr or .inr.gz (3D version) or .tif (3D-tiff version)
* `K` : proportionnal to the cell walls width [0.1 - 0.3]
* `sigma` : amount of gaussian blur [2 - 8]
* `gamma` : correct contrast ( <1:put more white / 1: no effect / >1:put more black)
* `upDiter` : the diffusion tensor is updated every upDiter iteration
* `nbIter` : number of iterations


### Test

------

```bash
ani3D t3_cut.inr.gz 0.2 3 1 10 50
```


